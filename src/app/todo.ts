export class Todo {
    title: string;
    description: string;
    deadline: Date;
}
