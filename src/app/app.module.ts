import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TodosComponent } from './todos/todos.component';
import { TodoDetailComponent } from './todo-detail/todo-detail.component';
import { CreateFormComponent } from './create-form/create-form.component';
import { TodoProvider } from './todo-provider';
import { CONFIG_URL, URL_TOKEN, ALIAS_TOKEN, FACTORY_TOKEN, myFactory, LOCALE_TOKEN, LOCALE } from './config';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';

const todoProviderFactory = (http) => {
    const p = new TodoProvider(http);
    p.load();
    return p;
};

@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    TodoDetailComponent,
    CreateFormComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
        { path: '', redirectTo: '/list', pathMatch: 'full' },
        { path: 'list', component: TodosComponent },
        { path: 'create', component: CreateFormComponent },
        { path: 'detail/:index', component: TodoDetailComponent },
        { path: '**', component: NotFoundComponent },
    ]),
  ],
  providers: [
      { provide: TodoProvider, useFactory: todoProviderFactory, deps: [HttpClient] },
      { provide: URL_TOKEN, useValue: CONFIG_URL},
      { provide: ALIAS_TOKEN, useExisting: URL_TOKEN},
      { provide: LOCALE_TOKEN, useValue: LOCALE },
      { provide: FACTORY_TOKEN, useFactory: myFactory, deps: [LOCALE_TOKEN]},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
