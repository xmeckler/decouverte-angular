import {Todo} from './todo';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import { TodosComponent } from './todos/todos.component';

@Injectable()
export class TodoProvider {
    data = new Map<number, Todo>();
    nextId = 1;

    constructor(private http: HttpClient) {}
    load(): void {
        this.http.get('/assets/todos.json')
        .subscribe(
            (data) => Object.entries(data).forEach(
                ([id, todo]) => {
                    this.data.set(+id, todo);
                    this.nextId = Math.max(1 + +id, this.nextId);
                }
            )
        );
    }

    add(todo: Todo) {
        console.log(todo);
        this.data.set(this.nextId, todo);
        this.nextId++;
        this.http.post('/assets/TodosComponent.json', todo).subscribe(() => this.load());
    }

    get(key: number) {
        return this.data.get(key);
    }
}
