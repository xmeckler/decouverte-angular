import { Component, OnInit, Input } from '@angular/core';
import { Todo } from '../todo';
import { ActivatedRoute } from '@angular/router';
import { TodoProvider } from '../todo-provider';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.css']
})
export class TodoDetailComponent implements OnInit {
  todo: Todo;
  id: number;
  constructor(
      private route: ActivatedRoute,
      private provider: TodoProvider
      ) { }

  ngOnInit() {
      this.route.paramMap.subscribe(
          params => this.todo = this.provider.get(+params.get('index'))
      );
  }
}
