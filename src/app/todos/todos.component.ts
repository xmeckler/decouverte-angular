import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Todo} from '../todo';
import {TodoProvider} from '../todo-provider';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos = new Map<number, Todo>();

  constructor(private provider: TodoProvider) {
      this.todos = this.provider.data;
   }

  ngOnInit() {
      this.provider.load();
  }
}
